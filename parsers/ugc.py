# ugc.py
# This is a parser that will strip the county/zone information from the NWS test
# product.
# Copyright ©2012 Steven DeWitt/mDetWeather

import re

# These are the state FIPS codes
_stateCodes = {
        'WA': '53', 'DE': '10', 'DC': '11', 'WI': '55', 'WV': '54', 'HI': '15',
        'FL': '12', 'WY': '56', 'PR': '72', 'NJ': '34', 'NM': '35', 'TX': '48',
        'LA': '22', 'NC': '37', 'ND': '38', 'NE': '31', 'TN': '47', 'NY': '36',
        'PA': '42', 'AK': '02', 'NV': '32', 'NH': '33', 'VA': '51', 'CO': '08',
        'CA': '06', 'AL': '01', 'AR': '05', 'VT': '50', 'IL': '17', 'GA': '13',
        'IN': '18', 'IA': '19', 'MA': '25', 'AZ': '04', 'ID': '16', 'CT': '09',
        'ME': '23', 'MD': '24', 'OK': '40', 'OH': '39', 'UT': '49', 'MO': '29',
        'MN': '27', 'MI': '26', 'RI': '44', 'KS': '20', 'MT': '30', 'MS': '28',
        'SC': '45', 'KY': '21', 'OR': '41', 'SD': '46'
}

# This is the pattern to strip the UGC code from NWS warnings/watches/advisories
_ident = r"[A-Z]{3}"
_number = r"[0-9]{3}"
_delim = r">|\-\n?"
_area = r"%s(?:%s(?:%s))+" % (_ident, _number, _delim)
#_time = r"[0-9]{6}"
_pattern = r"((?:%s)+)" % _area

_query = re.compile(_pattern)

# Returns a list of unformatted FIPS county codes
def parse(warning):
    results = _query.findall(warning)
    fipsCodes = str(results[0])
    newCodes = fipsCodes.replace('\n','')
    newList = newCodes.split('-')
    del newList[-1]
    return newList

# This will take the UGC data from the warning, and put it into a more graceful
# format in a list: [statecode, countycode]
# Make sure to run it through fips.py to get the actual state and county name
def state_county(ugcCodes):
    fipsCodes = []
    currentState = ''
    pattern = re.compile(r'^[A-Z][A-Z]C\d{3}$')
    groupPattern = re.compile(r'^([A-Z][A-Z])C(\d{3})$')
    for code in ugcCodes:
        if pattern.match(code):
            fips = groupPattern.match(code)
            currentState = stateCodes[fips.group(1).lstrip('0')]
            fipsCodes.append([currentState, fips.group(2).lstrip('0')])
        else:
            fipsCodes.append([currentState, code.lstrip('0')])
    return fipsCodes
