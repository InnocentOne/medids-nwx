# fips.py
# This takes the UGC codes and matches them to State and County names
# Copyright ©2012 Steven DeWitt/mDetWeather

import csv, sys

def Match(ugcItem):
    with open('parsers/codes.csv', 'rb') as file:
        fips = csv.reader(file)
        try:
            for row in fips:
                if ugcItem[0] == row[2] and ugcItem[1] == row[3]:
                    return [row[0], row[1]]
            return None
        except csv.Error, e:
            sys.exit(e)

def StateCountyNames(ugcCode):
    names = []
    for code in ugcCode:
        names.append(Match(code))
    return names
