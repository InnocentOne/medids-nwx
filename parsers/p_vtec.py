# p_vtec.py
# This will parse the P-VTEC information from NWS text data
# Copyright ©2012 Steven DeWitt/mDetWeather

import re

# This is the pattern that will match the P-VTEC line, and group it into a list
pclass = r'[O|T|E|X]'
action = r'[A-Z]{3}'
oid = r'[A-Z]{4}'
phenomena = r'[A-Z]{2}'
signif = r'[W|A|Y|S|F|O|N]'
etn = r'\d{4}'
date = r'\d{6}T\d{4}Z'

pattern = r'/(%s)\.(%s)\.(%s)\.(%s)\.(%s)\.(%s)\.(%s)\-(%s)/' % (pclass, action,
        oid, phenomena, signif, etn, date, date)

query = re.compile(pattern)

# TODO: A lot to do here

def parse(data):
    return query.findall(data)
