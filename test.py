from parsers import ugc

file = open('tests/ex4.txt')
text = file.read()

results = ugc.parse(text)

#for x in results:
#    if re.match(r'^[A-Z][A-Z][A-Z]\d{3}$', x):
#        y = re.match(r'^([A-Z][A-Z][A-Z])(\d{3})$', x)
#        print y.group(1)
#        print y.group(2)
#    else:
#        print x

newresults = ugc.state_county(results)

from parsers import fips

names = fips.state_county(newresults)

print 'Warning for:'
for name in names:
    print '%s county in %s state.' % (name[1], name[0])
